# A Python script that reads a text file containing Confluence space keys and page names,
# and reports on duplicate page names.
# The script assumes a text file of a specific format.
# See the README for a description of the expected file format.
#
# This script works in Python 2.7.

import string
pages_file = open("pages.txt", "r")
space_key = ""
page_name = ""
pages_list=[]
for line in pages_file:
    # Look for space keys
    if string.find(line, "Spacekey=", 0) != -1:
        # Remove string at beginning
        space_key = string.lstrip(line, "Spacekey=")
         
        space_key = string.rstrip(space_key, "\n")
    else:
        # This line contains a page name
        page_name = line
        # Remove new line character at end
        page_name = string.rstrip(page_name, "\n")
        # Add a new entry to our pages list.
        # Each entry consists of a list, containing the page name and the space key.
        # We will therefore have a list within a list.
        pages_list.append([page_name, space_key])
# Now we have built our list of pages and space keys.
# Let's sort the list by page name.
pages_list.sort()
# Find duplicate page names.
# And print the list of duplicate page names plus their space keys.
previous_page_name = ""
previous_line = ""
this_page_name = ""
previous_page_printed = "N"
for this_line in pages_list:
    # Get the page name out of the list.
    # And change the page name to upper case, because the comparison is case sensitive.
    this_page_name = string.upper(this_line[0])
    # Check if it's a duplicate of the previous page
    if this_page_name == previous_page_name:
        # Make sure we print the previous line too
        if previous_page_printed == "N":
            print ""
            print "Duplicate page: " + previous_page_name + " in space: " + previous_line[1]
            previous_page_printed = "Y"
        print "Duplicate page: " + this_page_name + " in space: " + this_line[1]
    else:
        previous_page_printed = "N"
    previous_page_name = this_page_name
    previous_line = this_line
