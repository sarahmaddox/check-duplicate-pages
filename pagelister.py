# A Python script that lists the names of all pages in a given set of Confluence spaces.
# It puts the page names and space keys into a text file of a specific format,
# as required by the "Check duplicate pages" script "pageduptest.py".
# See the README for a description of the expected file format.
#
# This script works with Python 3.2.3
# This script works with Confluence 4.0 or later. It uses version 2 of the Confluence XML-RPC API.
# Note: You could probably change it to use version 1 of the API, with Confluence 3. I have not tested that.
#
# Friendly warning: This script is provided "as is" and without any guarantees.
# I developed it to solve a specific problem.
# I'm sharing it because I hope it will be useful to others too.
# If you have any improvements to share, please let me know.
#
# Author: Sarah Maddox
# Source: https://bitbucket.org/sarahmaddox/check-duplicate-pages
# Usage guide: http://ffeathers.wordpress.com/2012/07/28/how-to-find-duplicate-page-names-across-confluence-spaces/

import xmlrpc.client

pages_list = ""
spacekeys = ""
spacekeys_list = []

# Get from input:  Confluence URL, username, password, list of space keys, output file name

print("G'day! I'm the pagelister Python script.\nGive me some Confluence spaces, and I'll list their pages.\n")
site_URL = input("Confluence site URL (exclude final slash): ")
username = input("Username: ")
pwd = input("Password: ")
spacekeys = input("Space keys (comma-separated, case insensitive, no spaces): ")
output_filename = input("Output file name (example: 'pages.txt'): ")

spacekeys_list = spacekeys.split(",")

# Open the output file for writing. Will overwrite existing file.
pages_file = open(output_filename, "w+")

# Log in to Confluence
server = xmlrpc.client.ServerProxy(site_URL + "/rpc/xmlrpc")
token = server.confluence2.login(username, pwd)

# For each space key, get the list of pages and write them out to a file

for spacekey in spacekeys_list:
    pages_file.write("Spacekey=" + spacekey + "\n")

    pages_list = server.confluence2.getPages(token, spacekey)
    pages_count = len(pages_list)

    for page in pages_list:
        pages_file.write(page["title"] + "\n")

pages_file.close()
print("All done! I've put the results in this file: ", output_filename)


