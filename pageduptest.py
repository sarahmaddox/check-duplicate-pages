# A Python script that reads a text file containing Confluence space keys and page names,
# and reports on duplicate page names.
# The script assumes a text file of a specific format.
# See the README for a description of the expected file format.
#
# This script works with Python 3.2.3
#
# Friendly warning: This script is provided "as is" and without any guarantees.
# I developed it to solve a specific problem.
# I'm sharing it because I hope it will be useful to others too.
# If you have any improvements to share, please let me know.
#
# Author: Sarah Maddox
# Source: https://bitbucket.org/sarahmaddox/check-duplicate-pages
# Usage guide: http://ffeathers.wordpress.com/2012/07/28/how-to-find-duplicate-page-names-across-confluence-spaces/

# Get from input: Input file name, output file name

print("G'day! I'm the pageduptest Python script.\nGive me an input text file of page names,\nand I'll list the duplicate pages to an output file.\n")
input_filename = input("Input file name (example: 'pages.txt'): ")
output_filename = input("Output file name (example: 'duppages.txt'): ")

# Open the input file for reading
pages_file = open(input_filename, "r")

# Open the output file for writing. Will overwrite existing file.
duppages_file = open(output_filename, "w+")

space_key = ""
page_name = ""
page_spaces = []
pages_dict={}
for line in pages_file:
    # Look for space keys
    if str.find(line, "Spacekey=", 0) != -1:
        # This line contains a space key
        # Remove string at beginning
        space_key = str.lstrip(line, "Spacekey=")
        # Remove new line character at end
        space_key = str.rstrip(space_key, "\n")
    else:
        # This line contains a page name. Get the page name.
        # And change the page name to upper case, because the comparison is case sensitive.
        page_name = str.upper(line)
        # Remove new line character at end
        page_name = str.rstrip(page_name, "\n")
        # Add a new entry to our pages dictionary.
        # Each entry consists of:
        #    a key, which is the page name.
        #    a value, which is a list of space keys for all the spaces that contain the page.
        # If the page is already in the dictionary, add the new space key to the list in the dictionary value
        if page_name in pages_dict:
            pages_dict[page_name].append(space_key)
        else:
            # Add the page name and space key to the dictionary
            pages_dict[page_name] = [space_key]

# Now we have built our dictionary of pages and space keys.
# To find duplicate page names, we just print all entries that have more than one space key.

for this_line in pages_dict:
    page_spaces = pages_dict[this_line]
    if len(page_spaces) > 1:
#       print(" ")
        duppages_file.write("\n")
        for space in page_spaces:
#           print("Duplicate page: " + this_line + " in space: " + space)
            duppages_file.write("Duplicate page: " + this_line + " in space: " + space + "\n")

pages_file.close()
duppages_file.close()
print("All done! I've put the results in this file: ", output_filename)
